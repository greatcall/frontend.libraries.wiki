# FrontEnd.Libraries
Contains customized front-end libraries such as Modernizr.

### How do I install it?
###### Modernizr
`install-package GreatCall.FrontEnd.Libraries.Modernizr`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/frontend.libraries.wiki/wiki/).
